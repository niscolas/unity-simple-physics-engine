using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsTests : MonoBehaviour
{
    [SerializeField]
    private Vector3 _velocity;

    [SerializeField]
    private Rigidbody _rigidbody;
    
    private void Start()
    {
        _rigidbody.velocity = _velocity;
    }
}

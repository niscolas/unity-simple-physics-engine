﻿using NaughtyAttributes;
using UnityEngine;

public class PhysicShape : MonoBehaviour
{
    [SerializeField]
    protected Vector3 _position;

    [SerializeField]
    protected bool _isStatic;

    [SerializeField]
    protected float _bounciness = 0.5f;

    [SerializeField]
    private ShapeType _type;
    
    [SerializeField]
    private bool _isActive = true;

    [ShowIf(nameof(IsSphereShape))]
    [SerializeField]
    private float _radius = 1;

    private bool IsPlaneShape => _type == ShapeType.Plane;

    private bool IsSphereShape => _type == ShapeType.Sphere;

    public float Radius
    {
        get => _radius;
        set => _radius = value;
    }

    public Vector3 Position
    {
        get => _position;
        set => _position = value;
    }

    public bool IsStatic
    {
        get => _isStatic;
        set => _isStatic = value;
    }

    public float Bounciness => _bounciness;

    public ShapeType Type => _type;

    public Vector3 PlaneNormal => _transform.up;

    public bool IsActive
    {
        get => _isActive;
        set => _isActive = value;
    }

    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
        _position = _transform.position;
    }
}
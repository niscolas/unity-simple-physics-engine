﻿using UnityEngine;

public readonly struct CollisionData
{
    public readonly Vector3 Normal;
    public readonly Vector3 Position;
    public readonly float Intersection;

    public CollisionData(
        Vector3 normal, Vector3 position, float intersection)
    {
        Normal = normal;
        Position = position;
        Intersection = intersection;
    }
}
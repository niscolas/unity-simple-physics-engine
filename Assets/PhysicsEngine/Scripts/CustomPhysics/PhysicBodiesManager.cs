﻿using System.Collections.Generic;
using UnityEngine;

public class PhysicBodiesManager : MonoBehaviour
{
    [Header("Debug")]
    [SerializeField]
    private List<PhysicBody> _bodies = new List<PhysicBody>();

    private void Awake()
    {
        PhysicBody.Enabled += PhysicBodyOnEnabled;
        PhysicBody.Disabled += PhysicBodyOnDisabled;
    }

    private void OnDestroy()
    {
        PhysicBody.Enabled -= PhysicBodyOnEnabled;
        PhysicBody.Disabled -= PhysicBodyOnDisabled;
    }

    private void PhysicBodyOnEnabled(PhysicBody physicBody)
    {
        _bodies.Add(physicBody);
    }

    private void PhysicBodyOnDisabled(PhysicBody physicBody)
    {
        _bodies.Remove(physicBody);
    }

    private void FixedUpdate()
    {
        SimulateBodiesPhysics();
    }

    private void SimulateBodiesPhysics()
    {
        for (int i = 0; i < _bodies.Count; i++)
        {
            SimulateBodyPhysics(_bodies[i]);
        }
    }

    private void SimulateBodyPhysics(PhysicBody body)
    {
        if (!body || body.Shape.IsStatic)
        {
            return;
        }

        body.PhysicUpdate(Time.fixedDeltaTime);

        for (int i = 0; i < _bodies.Count; i++)
        {
            PhysicBody otherBody = _bodies[i];

            if (body.Equals(otherBody) ||
                !body.Shape.IsActive ||
                !otherBody.Shape.IsActive ||
                !CheckCollision(
                    body,
                    otherBody,
                    out CollisionData collision))
            {
                continue;
            }

            OnBodiesCollision(body, otherBody, collision);
        }
    }

    public static bool CheckCollision(
        PhysicBody body, PhysicBody otherBody, out CollisionData collision)
    {
        collision = default;

        if (!TryIdentifyCollision(body.Shape, otherBody.Shape, out CollisionType collisionType))
        {
            return false;
        }

        switch (collisionType)
        {
            case CollisionType.SphereSphere:
                return CheckSphereWithSphereCollision(body.Shape, otherBody.Shape, out collision);

            case CollisionType.PlaneSphere:
                return CheckPlaneWithSphereCollision(body, otherBody, out collision);

            case CollisionType.SpherePlane:
                return CheckPlaneWithSphereCollision(otherBody, body, out collision);
        }

        return false;
    }

    public static bool TryIdentifyCollision(
        PhysicShape shape, PhysicShape otherShape, out CollisionType collisionType)
    {
        collisionType = default;

        if (shape.Type == ShapeType.Sphere && otherShape.Type == ShapeType.Sphere)
        {
            collisionType = CollisionType.SphereSphere;
            return true;
        }

        if (shape.Type == ShapeType.Sphere && otherShape.Type == ShapeType.Plane)
        {
            collisionType = CollisionType.SpherePlane;
            return true;
        }

        if (shape.Type == ShapeType.Plane && otherShape.Type == ShapeType.Sphere)
        {
            collisionType = CollisionType.PlaneSphere;
            return true;
        }

        return false;
    }

    public static bool CheckSphereWithSphereCollision(
        PhysicShape shape, PhysicShape otherShape, out CollisionData collision)
    {
        collision = default;

        Vector3 direction = shape.Position - otherShape.Position;
        float intersection = shape.Radius + otherShape.Radius - direction.magnitude;

        if (intersection < 0)
        {
            return false;
        }

        Vector3 normal = direction.normalized;
        Vector3 position = otherShape.Position + normal * otherShape.Radius;

        collision = new CollisionData(normal, position, intersection);

        return true;
    }

    public static bool CheckPlaneWithSphereCollision(
        PhysicBody planeBody, PhysicBody sphereBody, out CollisionData collision)
    {
        collision = default;

        PhysicShape planeShape = planeBody.Shape;
        PhysicShape sphereShape = sphereBody.Shape;

        Vector3 planeNormal = planeShape.PlaneNormal.normalized;
        Vector3 sphereCenter = sphereShape.Position;
        Vector3 collisionDirection = sphereCenter - planeShape.Position;
        float d = Vector3.Dot(collisionDirection, planeNormal);
        float intersection = sphereShape.Radius - d;

        if (intersection < 0)
        {
            return false;
        }

        Vector3 position = sphereCenter - planeNormal * intersection;

        Vector3 collisionNormal = Vector3.Reflect(
            sphereBody.Velocity, planeNormal).normalized;
        collision = new CollisionData(collisionNormal, position, intersection);

        return true;
    }

    private static void OnBodiesCollision(
        PhysicBody body, PhysicBody otherBody, CollisionData collision)
    {
        body.OnCollision(new BodyCollision(collision, otherBody));
        otherBody.OnCollision(new BodyCollision(collision, body));
        
        ValidateConstraints(body, otherBody, collision);
        ResolveCollision(body, otherBody, collision);
    }

    private static void ValidateConstraints(
        PhysicBody body, PhysicBody otherBody, CollisionData collision)
    {
        if (otherBody.Shape.IsStatic)
        {
            body.Shape.Position += collision.Normal * collision.Intersection;
        }
        else
        {
            float totalMass = body.Mass + otherBody.Mass;

            Vector3 multiplyFactor = collision.Normal * collision.Intersection;

            body.Shape.Position += multiplyFactor * (otherBody.Mass / totalMass);
            otherBody.Shape.Position -= multiplyFactor * (body.Mass / totalMass);
        }
    }

    private static void ResolveCollision(
        PhysicBody body, PhysicBody otherBody, CollisionData collision)
    {
        PhysicShape bodyShape = body.Shape;
        PhysicShape otherBodyShape = otherBody.Shape;

        if (otherBodyShape.IsStatic)
        {
            body.Velocity = collision.Normal * (body.Velocity.magnitude * bodyShape.Bounciness);
        }
        else
        {
            Vector3 multiplyFactor = collision.Normal * (body.Velocity + otherBody.Velocity).magnitude;

            body.Velocity = multiplyFactor * bodyShape.Bounciness;
            otherBody.Velocity = multiplyFactor * otherBodyShape.Bounciness;
        }
    }
}
﻿public enum CollisionType
{
    SphereSphere,
    PlaneSphere,
    SpherePlane
}
﻿using System;
using UnityEngine;

public class PhysicBody : BetterMonoBehaviour
{
    [SerializeField]
    private PhysicShape _shape;

    [SerializeField]
    private float _mass = 1;

    [SerializeField]
    private Vector3 _velocity;

    [SerializeField]
    private Vector3 _force;

    [SerializeField]
    private bool _hasGravity = true;

    [SerializeField]
    private Vector3 _gravity = new Vector3(0, -9.81f, 0);

    public float Mass => _mass;

    public Vector3 Velocity
    {
        get => _velocity;
        set => _velocity = value;
    }

    public bool IsMoving => _velocity.magnitude > float.Epsilon;

    public PhysicShape Shape => _shape;

    public bool HasGravity
    {
        get => _hasGravity;
        set => _hasGravity = value;
    }

    public static event Action<PhysicBody> Enabled;
    public static event Action<PhysicBody> Disabled;

    public event Action<BodyCollision> Collided;

    private void Reset()
    {
        TryGetComponent(out _shape);
    }

    private void OnEnable()
    {
        
        Enabled?.Invoke(this);
    }

    private void OnDisable()
    {
        Disabled?.Invoke(this);
    }

    public void PhysicUpdate(float deltaTime)
    {
        Vector3 acceleration = _force / _mass;

        if (_hasGravity)
        {
            acceleration += _gravity;
        }

        _velocity += acceleration * deltaTime;
        _shape.Position += _velocity * deltaTime;
        
        _transform.position = _shape.Position;
    }

    public void AddForce(Vector3 force)
    {
        _force = force;
    }

    public void AddVelocity(Vector3 velocity)
    {
        _velocity = velocity;
    }

    public void OnCollision(BodyCollision collision)
    {
        // Debug.Log($"{gameObject.name} collided with {collision.OtherBody.gameObject.name}");
        Collided?.Invoke(collision);
    }
}
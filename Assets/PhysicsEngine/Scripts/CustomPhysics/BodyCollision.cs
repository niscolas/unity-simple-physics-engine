﻿public readonly struct BodyCollision
{
    public readonly CollisionData Data;
    public readonly PhysicBody OtherBody;

    public BodyCollision(CollisionData data, PhysicBody otherBody)
    {
        Data = data;
        OtherBody = otherBody;
    }
}
﻿using System;
using Cysharp.Threading.Tasks;
using NaughtyAttributes;
using UnityEngine;

public class Canon : MonoBehaviour
{
    [SerializeField]
    private PhysicBody[] _projectilePrefabs;

    [SerializeField]
    private float _shotForce;

    [SerializeField]
    private Transform _shotSpawnPoint;

    [SerializeField]
    private float _reloadDuration;

    private readonly System.Random _random = new System.Random();
    private PhysicBody _currentAmmo;
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        Reload();
    }

    private void Update()
    {
        ShootInput();
    }

    private void ShootInput()
    {
        if (!Input.GetMouseButtonUp(0) && !Input.GetKeyUp(KeyCode.Space))
        {
            return;
        }

        Shoot();
    }

    private void Reload()
    {
        _currentAmmo = Instantiate(
            GetProjectilePrefab(),
            _shotSpawnPoint.position,
            Quaternion.identity);

        _currentAmmo.enabled = false;
    }

    private async UniTaskVoid ReloadAsync()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(_reloadDuration));

        Reload();
    }

    [Button]
    private void Shoot()
    {
        _currentAmmo.enabled = true;
        _currentAmmo.AddVelocity(_transform.forward * _shotForce);
        
        ReloadAsync().Forget();
    }

    private PhysicBody GetProjectilePrefab()
    {
        return _projectilePrefabs[_random.Next(0, _projectilePrefabs.Length)];
    }
}
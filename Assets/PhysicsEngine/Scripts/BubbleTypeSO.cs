﻿using UnityEngine;

[CreateAssetMenu(
    menuName = Constants.CreateAssetMenuPrefix + "Bubble Type",
    order = Constants.CreateAssetMenuOrder)]
public class BubbleTypeSO : ScriptableObject { }
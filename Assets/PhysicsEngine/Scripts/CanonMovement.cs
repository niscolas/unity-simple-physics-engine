﻿using UnityEngine;

public class CanonMovement : BetterMonoBehaviour
{
    [SerializeField]
    private float _turnSpeed;

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            RotateTo(-1);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
           RotateTo(1);
        }
    }

    private void RotateTo(float direction)
    {
        _transform.RotateAround(_transform.position, _transform.up, _turnSpeed * direction);
    }
}
﻿using System;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;
    
    [SerializeField]
    private float _force;
    
    [SerializeField]
    private ForceMode _forceMode;

    private Transform _transform;
    private float _startTime;
    
    private void Awake()
    {
    _startTime = Time.time;
        _transform = transform;
    }

    private void Start()
    {
        _rigidbody.velocity = _transform.forward * _force;
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(Time.time - _startTime);
    }
}
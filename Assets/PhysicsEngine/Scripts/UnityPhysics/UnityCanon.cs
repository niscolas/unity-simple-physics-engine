using UnityEngine;

public class UnityCanon : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _shotPrefab;

    [SerializeField]
    private Transform _spawnPoint;

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        Instantiate(_shotPrefab, _spawnPoint.position, _spawnPoint.rotation);
    }
}
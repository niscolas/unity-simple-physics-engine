﻿using UnityEngine;
using Random = System.Random;

public class BubleSpawner : MonoBehaviour
{
    [SerializeField]
    private Bubble[] _bubblePrefabs;

    [SerializeField]
    private int _rowCount;

    [SerializeField]
    private int _bubbleCountByRow;

    [SerializeField]
    private float _rowOffset;

    [SerializeField]
    private float _bubbleOffset;

    [SerializeField]
    private Transform _startPoint;

    private readonly Random _random = new Random();
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        if (!_startPoint)
        {
            _startPoint = _transform;
        }

        Spawn();
    }

    public void Spawn()
    {
        Vector3 startPosition = _startPoint.position;
        Vector3 currentPosition = startPosition;

        for (int i = 0; i < _rowCount; i++)
        {
            currentPosition.x = GetRowPositionX(startPosition.x, i);
            SpawnBubbleRow(currentPosition, i);
            currentPosition.y -= _rowOffset;
        }
    }

    private float GetRowPositionX(float defaultPositionX, int rowIndex)
    {
        if (rowIndex % 2 == 0)
        {
            return defaultPositionX;
        }
        else
        {
            return defaultPositionX + _bubbleOffset * 0.5f;
        }
    }

    private void SpawnBubbleRow(Vector3 startPosition, int rowIndex)
    {
        Vector3 currentPosition = startPosition;

        for (int i = 0; i < _bubbleCountByRow; i++)
        {
            SpawnBubble(currentPosition, rowIndex, i);
            currentPosition.x += _bubbleOffset;
        }
    }

    private void SpawnBubble(Vector3 currentPosition, int rowIndex, int columnIndex)
    {
        Bubble bubble = Instantiate(
            GetRandomBubblePrefab(),
            currentPosition,
            Quaternion.identity,
            _transform);
        
        bubble.PhysicBody.Shape.IsStatic = true;
        bubble.name = $"TargetBubble[{rowIndex}][{columnIndex}]";
    }

    private Bubble GetRandomBubblePrefab()
    {
        return _bubblePrefabs[_random.Next(0, _bubblePrefabs.Length)];
    }
}
﻿using UnityEngine;

public class PhysicBodyDestroyer : MonoBehaviour
{
    [SerializeField]
    private PhysicBody _physicBody;

    private void Start()
    {
        _physicBody.Collided += PhysicBody_OnCollided;
    }

    private void OnDestroy()
    {
        _physicBody.Collided -= PhysicBody_OnCollided;
    }

    private void PhysicBody_OnCollided(BodyCollision collision)
    {
        Destroy(collision.OtherBody.gameObject);
    }
}
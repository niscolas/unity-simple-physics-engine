﻿using System;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    private Camera _camera;
    private Transform _transform;
    private Transform _lookAtTransform;
    private Plane _plane;

    private void Awake()
    {
        _transform = transform;
        _plane = new Plane(-Vector3.forward, 0);
    }

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Ray ray = _camera.ScreenPointToRay(mousePosition);
        
        if (!_plane.Raycast(ray, out float distance))
        {
            return;
        }
        
        _transform.LookAt(ray.GetPoint(distance), _transform.up);
    }

    // private void FaceMouse()
    // {
    //     Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
    //     if (!Physics.Raycast(ray, out RaycastHit hit, 1000, _layerMask))
    //     {
    //         return;
    //     }
    //
    //     Vector3 lookAtPosition = hit.point;
    //     _transform.LookAt(lookAtPosition);
    // }
}
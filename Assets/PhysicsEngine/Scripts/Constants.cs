﻿public static class Constants
{
    public const int CreateAssetMenuOrder = -100;
    public const string ProjectName = "Physics Engine";
    public const string CreateAssetMenuPrefix = "[" + ProjectName + "]/";
}
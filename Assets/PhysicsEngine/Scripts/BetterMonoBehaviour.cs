﻿using UnityEngine;

public abstract class BetterMonoBehaviour : MonoBehaviour
{
    protected GameObject _gameObject;
    protected Transform _transform;
    
    protected virtual void Awake()
    {
        _gameObject = gameObject;
        _transform = transform;
    }
    //
    // protected void OnEnable()
    // {
    // }
    //
    // private async UniTaskVoid CallDelayedOnEnable()
    // {
    //     
    // }
    //
    // protected abstract void DelayedOnEnable();
}
﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class CanonBallTracker : MonoBehaviour
{
    [SerializeField]
    private PhysicBody _physicBody;
    
    private Transform _transform;
    private Vector3 _initialPosition;
    private bool _reachedMaxHeight;
    private float _initialHeight;
    private float _initialTime;
    private float _currentHeight;

    private void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        _initialPosition = _transform.position;
        _initialHeight =_initialPosition.y;
        _initialTime = Time.time;
        _currentHeight = _initialHeight;
    }

    private void FixedUpdate()
    {
        CheckMaxHeight();
        CheckMinHeight();
    }

    private void CheckMaxHeight()
    {
        float newHeight = _transform.position.y;
        if (!_reachedMaxHeight && newHeight < _currentHeight)
        {
            Debug.Log($"Reached max Height: {_currentHeight}");
            _reachedMaxHeight = true;
        }
        else
        {
            _currentHeight = newHeight;
        }
    }

    private void CheckMinHeight()
    {
        if (!(_transform.position.y <= _initialHeight))
        {
            return;
        }

        Debug.Log(
            $"Reached Ground in: {Time.time - _initialTime} seconds");
        
        Debug.Log(
            $"Horizontal Distance Traveled: {_transform.position.x - _initialPosition.x}");
    }
}
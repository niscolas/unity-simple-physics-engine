﻿using System;
using UnityEngine;

public class Bubble : BetterMonoBehaviour
{
    [SerializeField]
    private BubbleTypeSO _type;

    [SerializeField]
    private PhysicBody _physicBody;

    public BubbleTypeSO Type => _type;

    public PhysicBody PhysicBody => _physicBody;

    private void Start()
    {
        _physicBody.Collided += PhysicBody_OnCollided;
    }

    private void OnDestroy()
    {
        _physicBody.Collided -= PhysicBody_OnCollided;
    }

    private void PhysicBody_OnCollided(BodyCollision collision)
    {
        if (!collision.OtherBody.TryGetComponent(out Bubble otherBubble))
        {
            return;
        }

        if (_physicBody.Shape.IsStatic)
        {
            return;
        }

        if (otherBubble._type == _type)
        {
            _physicBody.enabled = false;
            _transform.SetParent(otherBubble._transform);
            
            otherBubble._physicBody.HasGravity = true;
            otherBubble._physicBody.Shape.IsStatic = false;
            otherBubble._physicBody.Shape.enabled = false;
            
            DestroyImmediate(this);
            DestroyImmediate(otherBubble);
        }
        else
        {
            _physicBody.Shape.IsStatic = true;
        }
    }
}